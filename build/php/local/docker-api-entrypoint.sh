#!/usr/bin/env sh
set -e

# If no command was passed, proceed to initial boot-up and run PHP-FPM, otherwise execute the command.
if [[ -z "$@" ]]; then
    if [[ -z "$API_SRC_DIR" ]]; then
        echo "Error: no source directory set. Set the API_SRC_DIR variable"
        exit 1
    fi

    cd $API_SRC_DIR

    if [[ -f "composer.json" ]]; then
        echo "Installing dependencies"
        composer install

    fi

    echo "Running PHP-FPM"
    exec php-fpm
else
    exec $@
fi