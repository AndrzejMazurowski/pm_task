#!/usr/bin/env sh
set -e

if [[ -z "$NGINX_HOST" ]]; then
    echo "NGINX_HOST variable is not set"
    exit 1
fi

echo "Configuring host for $NGINX_HOST"
envsubst '\$NGINX_HOST' < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf

echo "Starting nginx"
exec nginx -g "daemon off;";
