<?php


namespace App\Task\Application;


class RandomStringGenerator implements RandomStringGeneratorInterface
{
    /**
     * @var string
     */
    private $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public function getRandomStrings(int $count, int $minLength = 2, int $maxLength = 100): array
    {
        $randomStrings = [];
        for ($i = 0; $i < $count; $i++) {
            $length = rand($minLength, $maxLength);
            $randomStrings[] = $this->getRandomString($length);
        }

        return $randomStrings;
    }

    private function getRandomString(int $length): string
    {
        $charactersLength = strlen($this->characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $this->characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}