<?php


namespace App\Task\Application;


interface RandomStringGeneratorInterface
{
    public function getRandomStrings(int $count, int $minLength = 2, int $maxLength = 100): array;
}