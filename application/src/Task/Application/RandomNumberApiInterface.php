<?php

namespace App\Task\Application;

interface RandomNumberApiInterface
{
    /**
     * @param int $count
     * @param int $min
     * @param int $max
     * @return array
     */
    public function getRandomNumbers(int $count, int $min = 0, int $max = 80): array;
}