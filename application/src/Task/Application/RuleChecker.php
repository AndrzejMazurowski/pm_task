<?php

namespace App\Task\Application;

use App\Task\Domain\Rules\RuleInterface;

class RuleChecker implements RuleCheckerInterface
{
    /**
     * @var array
     */
    private $results = [];

    /**
     * @var RuleInterface[]
     */
    private $rules;

    /**
     * @param array $rules
     */
    public function setRules(array $rules): void
    {
        $this->rules = $rules;
    }

    /**
     * @param array $collectionA
     * @param array $collectionB
     */
    public function checkRules(array $collectionA, array $collectionB)
    {
        foreach ($this->rules as $key => $rule) {
            $this->results[$key] = $rule($collectionA, $collectionB);
        }
    }

    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }
}