<?php

namespace App\Task\Application;

use App\Shared\Application\CommandInterface;

class AppRunCommand implements CommandInterface
{
    /**
     * @var int
     */
    private $count;

    /**
     * AppRunCommand constructor.
     * @param int $count
     */
    public function __construct(int $count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}