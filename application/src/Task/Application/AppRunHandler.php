<?php

namespace App\Task\Application;

use App\Shared\Application\CommandInterface;
use App\Shared\Application\HandlerInterface;
use App\Task\Domain\Rules\XRule;
use App\Task\Domain\Rules\YRule;
use App\Task\Domain\Rules\ZRule;
use App\Task\Infrastructure\RandomNumberApi;

class AppRunHandler implements HandlerInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(CommandInterface $cmd)
    {
        $count = $cmd->getCount();

        $collectionA = $this->getCollectionA($count);
        $collectionB = $this->getCollectionB($count);

        $rulesResult = $this->checkRules($collectionA, $collectionB);

        $this->saveData($collectionA, $collectionB, $rulesResult);
    }

    /**
     * @inheritDoc
     */
    public function getCommandName(): string
    {
        return AppRunCommand::class;
    }

    /**
     * @param int $count
     *
     * @return array
     */
    private function getCollectionA(int $count): array
    {
        $randomNumberGenerator = new RandomNumberApi();

        return $randomNumberGenerator->getRandomNumbers($count);
    }

    /**
     * @param int $count
     *
     * @return array
     */
    private function getCollectionB(int $count): array
    {
        $randomStringGenerator = new RandomStringGenerator();
        return $randomStringGenerator->getRandomStrings($count);
    }

    /**
     * @param array $collectionA
     * @param array $collectionB
     *
     * @return array
     */
    private function checkRules(array $collectionA, array $collectionB): array
    {
        $rulesChecker = new RuleChecker();

        $rulesChecker->setRules([
            'x' => new XRule(),
            'y' => new YRule(),
            'z' => new ZRule(),
        ]);

        $rulesChecker->checkRules($collectionA, $collectionB);

        return $rulesChecker->getResults();
    }

    private function saveData(array $collectionA, array $collectionB, array $rulesResult)
    {
        $array = [
            'collectionA' => $collectionA,
            'collectionB' => $collectionB,
            'results' => $rulesResult,
        ];

        $json = json_encode($array);

        //@ToDo totally remake it using symfony filesystem
        file_put_contents('public/results/' . time() . '.json', $json);
    }
}