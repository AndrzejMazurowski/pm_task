<?php

declare(strict_types=1);

namespace App\Task\Infrastructure;

use App\Task\Application\RandomNumberApiInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RandomNumberApi implements RandomNumberApiInterface
{
    private const ENTRYPOINT = 'http://www.randomnumberapi.com/api/v1.0/';

    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * OpenWeatherApi constructor.
     */
    public function __construct()
    {
        $this->client = HttpClient::createForBaseUri(self::ENTRYPOINT);
    }

    /**
     * @param int $count
     * @param int $min
     * @param int $max
     * @return array
     */
    public function getRandomNumbers(int $count, int $min = 0, int $max = 80): array
    {
        return $this->callApi(
            'GET',
            'random',
            [
                'min' => $min,
                'max' => $max,
                'count' => $count
            ]
        );
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $arguments
     *
     * @return array
     */
    private function callApi(string $method, string $url, array $arguments = []): array
    {
        try {
            $response = $this->client->request($method, $url, ['query' => array_filter($arguments)]);
            return json_decode($response->getContent(), true);
        } catch (ExceptionInterface $e) {
            // @ToDo handle exceptions
            return [];
        }
    }
}