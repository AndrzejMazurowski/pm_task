<?php

namespace App\Task\Infrastructure;

use App\Shared\Application\HandlerInterface;
use App\Task\Application\AppRunCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RunCommand extends Command
{
    /**
     * @var HandlerInterface
     */
    private $handler;

    public function __construct(HandlerInterface $handler)
    {
        $this->handler = $handler;

        parent::__construct('app:run');
    }

    protected function configure()
    {
        $this->addArgument(
            'collection_size',
            InputArgument::REQUIRED,
            'Collection size requested to create (range 5 - 1000)'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = (int) $input->getArgument('collection_size');

        if (!$count) {
            $output->writeln('<error>Non-numeric argument or zero given</error>');
            return Command::FAILURE;
        }

        $command = new AppRunCommand($count);
        ($this->handler)($command);

        return Command::SUCCESS;
    }
}