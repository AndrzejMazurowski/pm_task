<?php

namespace App\Task\Domain\Rules;

class ZRule implements RuleInterface
{
    /**
     * @param array $collectionA
     * @param array $collectionB
     *
     * @return array
     */
    public function __invoke(array $collectionA, array $collectionB): array
    {
        $results = [];

        foreach ($collectionA as $key => $value) {
            $results[$key] = $this->checkRule($value, '');
        }

        return $results;
    }

    /**
     * @param int $valueA
     * @param string $valueB
     *
     * @return int
     */
    public function checkRule(int $valueA, string $valueB): int
    {
        return $valueA > 35 ? 7 : ($valueA < 15 ? 3 : 0);
    }
}