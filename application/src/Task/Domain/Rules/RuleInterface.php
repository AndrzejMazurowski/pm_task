<?php

namespace App\Task\Domain\Rules;

interface RuleInterface
{
    /**
     * @param array $collectionA
     * @param array $collectionB
     *
     * @return array
     */
    public function __invoke(array $collectionA, array $collectionB): array;

    /**
     * @param int $valueA
     * @param string $valueB
     *
     * @return int
     */
    public function checkRule(int $valueA, string $valueB): int;
}