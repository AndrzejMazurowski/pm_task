<?php

namespace App\Task\Domain\Rules;

class YRule implements RuleInterface
{
    /**
     * @param array $collectionA
     * @param array $collectionB
     *
     * @return array
     */
    public function __invoke(array $collectionA, array $collectionB): array
    {
        $results = [];

        foreach (array_keys($collectionA) as $key) {
            $results[$key] = $this->checkRule($collectionA[$key], $collectionB[$key]);
        }

        return $results;
    }

    /**
     * @param int $valueA
     * @param string $valueB
     *
     * @return int
     */
    public function checkRule(int $valueA, string $valueB): int
    {
        $digitCount = preg_match_all('/\d/', $valueB);

        return $digitCount > $valueA ? 1 : 2;
    }
}