<?php


namespace App\Shared\Application;


interface HandlerInterface
{
    /**
     * @param CommandInterface $cmd
     * @return mixed
     */
    public function __invoke(CommandInterface $cmd);

    /**
     * @return string
     */
    public function getCommandName(): string;
}