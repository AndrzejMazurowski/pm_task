# Performance Media - Recruitment Task
## Table of contents
* [Description](#description)
* [How to build](#how_to_build)
* [How to run](#how_to_run)
* [Changelog](#changelog)

## Description
Task was to create 2 collections with given size - one with random numbers (named *collection A*) and second with random strings (named *collection B*). After that process should check 3 rules described under for each pair of values from collections.

Rule X
* if value from *collection A* is greater than value length from *collection B" then result is 1
* if value from *collection A* is lesser than value length from *collection B" then result is 2

Rule Y
* if digit count from *collection B* is greater than value from *collection A" then result is 1
* else result is 2

Rule Z
* if value from *collection A* is greater than 37 then result is 7
* if value from *collection A* is lesser than 15 then result is 3


## How to build
Project is dockerized with using of BUILDKIT. 
It requires *Docker* in version (18.09 or higher) and *Docker Compose* in version (1.25.1 or higher).

Ideally for it is to make some aliases to easier container building and running.
```
alias dc='COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose'
alias dcu='COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose up -d'
```

after that just execute
```
$ dcu
```


## How to run

To run process simply run symfony command in terminal

```
$ docker-compose exec api sh
$ console/bin app:run <collection_size>
```

after that process should create file named after timestamp in location

```
project_location/application/public/results/
```

## Changelog

* v.0.1
> - initial task instance
> - working requested process
> 
> @ToDo
> - better file saving management
> 